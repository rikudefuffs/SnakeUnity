using UnityEngine;

public class Wall : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == Snake.instance.gameObject)
        {
            Snake.instance.Die();
        }
    }
}
