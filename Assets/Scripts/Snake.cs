using System;
using System.Collections.Generic;
using UnityEngine;

public class Snake : MonoBehaviour
{
    public static Snake instance;

    public event Action OnDeath;

    public float speed;
    public Transform snakePiecePrefab;
    List<Transform> snakePieces;
    Vector3 destination;
    Vector3 direction;
    bool alive;

    void Awake()
    {
        if (instance != null
        && instance != this)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
        snakePieces = new List<Transform>();
    }

    void Start()
    {
        OnGameBegin();
    }

    void OnGameBegin()
    {
        snakePieces.Clear();
        snakePieces.Add(transform);
        destination = transform.position;
        direction = Vector3.right;
        alive = true;
    }

    void Update()
    {
        if (!alive)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            direction = Vector3.up;
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            direction = Vector3.down;
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            direction = Vector3.left;
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            direction = Vector3.right;
        }

        transform.position = Vector3.MoveTowards(transform.position, destination, speed * Time.deltaTime);

        if (transform.position != destination)
        {
            return;
        }

        destination += direction;
        SetBodyPartsTargetPosition();
    }

    void SetBodyPartsTargetPosition()
    {
        for (int i = snakePieces.Count - 1; i > 0; i--)
        {
            snakePieces[i].GetComponent<SnakeBody>().SetTargetPosition(snakePieces[i - 1].position);
        }
    }

    public void Grow()
    {
        Transform newPiece = Instantiate(snakePiecePrefab, snakePieces[snakePieces.Count - 1].transform.position, Quaternion.identity);
        newPiece.gameObject.GetComponent<SnakeBody>().Initialize(snakePieces.Count == 1);
        snakePieces.Add(newPiece);
    }

    public void Die()
    {
        destination = transform.position;
        alive = false;
        GetComponent<BoxCollider2D>().enabled = false;
        OnDeath?.Invoke();
    }

    public void Respawn()
    {
        ScoreManager.instance.ResetScore();
        for (int i = snakePieces.Count - 1; i > 0; i--)
        {
            Destroy(snakePieces[i].gameObject);
        }
        transform.position = Vector3.zero;
        GetComponent<BoxCollider2D>().enabled = true;
        OnGameBegin();
    }
}