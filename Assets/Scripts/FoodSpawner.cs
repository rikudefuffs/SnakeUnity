using UnityEngine;

public class FoodSpawner : MonoBehaviour
{
    public static FoodSpawner instance;
    public Food foodPrefab;
    BoxCollider2D spawnArea;

    void Awake()
    {
        if (instance != null
        && instance != this)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
        spawnArea = GetComponent<BoxCollider2D>();
        SpawnFood();
    }

    void SpawnFood()
    {
        Bounds bounds = spawnArea.bounds;
        float xCoordinate = Random.Range(bounds.min.x, bounds.max.x);
        float yCoordinate = Random.Range(bounds.min.y, bounds.max.y);
        Food food = Instantiate(foodPrefab, new Vector3(xCoordinate, yCoordinate, 0), Quaternion.identity);
        food.OnCollected += OnFoodCollected;
    }

    void OnFoodCollected()
    {
        SpawnFood();
        ScoreManager.instance.SetScore(ScoreManager.instance.score + 1);
        Snake.instance.Grow();
    }
}
