using System.Collections;
using UnityEngine;

public class SnakeBody : MonoBehaviour
{
    Vector3 destination;

    internal void Initialize(bool ignoreColliderLogic)
    {
        destination = transform.position;
        if (ignoreColliderLogic)
        {
            return;
        }
        StartCoroutine(EnableCollider(1));
    }

    IEnumerator EnableCollider(float delay)
    {
        yield return new WaitForSeconds(delay);
        GetComponent<BoxCollider2D>().enabled = true;
    }

    public void SetTargetPosition(Vector3 targetPosition)
    {
        destination = targetPosition;
    }

    void LateUpdate()
    {
        transform.position = Vector3.MoveTowards(transform.position, destination, Snake.instance.speed * Time.deltaTime);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == Snake.instance.gameObject)
        {
            Snake.instance.Die();
        }
    }
}
