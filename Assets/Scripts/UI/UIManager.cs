using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;
    public GameObject gameOverUI;

    void Awake()
    {
        if (instance != null
        && instance != this)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
    }

    void Start()
    {
        gameOverUI.SetActive(false);
        Snake.instance.OnDeath += OnSnakeDeath;
    }

    void OnSnakeDeath()
    {
        gameOverUI.SetActive(true);
    }

    void OnDestroy()
    {
        Snake.instance.OnDeath -= OnSnakeDeath;
    }
}
