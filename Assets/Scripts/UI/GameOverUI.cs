using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameOverUI : MonoBehaviour
{
    public TMP_Text lblScore;
    public Button btnRespawn;
    public Button btnQuit;

    void Start()
    {
        btnRespawn.onClick.RemoveAllListeners();
        btnRespawn.onClick.AddListener(OnClickRespawn);
        btnQuit.onClick.RemoveAllListeners();
        btnQuit.onClick.AddListener(OnClickQuit);
    }

    void OnEnable()
    {
        lblScore.text = "Score: " + ScoreManager.instance.score;
    }

    void OnClickRespawn()
    {
        gameObject.SetActive(false);
        Snake.instance.Respawn();
    }

    void OnClickQuit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        return;
#else
        Application.Quit();
#endif
    }
}
