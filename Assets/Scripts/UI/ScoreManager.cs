using TMPro;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager instance;
    public TMP_Text lblScore;
    public int score = 0;

    void Awake()
    {
        if (instance != null
        && instance != this)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
        score = 0;
    }

    void Start()
    {
        RefreshScore();
    }

    public void SetScore(int newScore)
    {
        score = newScore;
        RefreshScore();
    }


    void RefreshScore()
    {
        lblScore.text = "Score: " + score;
    }

    public void ResetScore()
    {
        SetScore(0);
    }
}
