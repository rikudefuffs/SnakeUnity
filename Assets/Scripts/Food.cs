using System;
using UnityEngine;

public class Food : MonoBehaviour
{
    public event Action OnCollected;

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == Snake.instance.gameObject)
        {
            OnCollected?.Invoke();
            Destroy(gameObject);
        }
    }

    void OnDestroy()
    {
        OnCollected = null;
    }
}
